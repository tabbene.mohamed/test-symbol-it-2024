const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const sequelize = require('./config/database');
const excuseRoutes = require('./routes/excuses');

const app = express();

require('dotenv').config();

app.use(cors());
app.use(bodyParser.json());

app.use('/excuses', excuseRoutes);

const PORT = process.env.PORT || 5000;

sequelize.sync().then(() => {
  app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  });
}).catch(error => {
  console.error('Unable to connect to the database:', error);
});
