const { DataTypes } = require('sequelize');
const sequelize = require('../config/database');

const Excuse = sequelize.define('Excuse', {
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true
  },
  http_code: {
    type: DataTypes.INTEGER,
    allowNull: false
  },
  tag: {
    type: DataTypes.STRING,
    allowNull: false
  },
  message: {
    type: DataTypes.STRING,
    allowNull: false
  }
});

module.exports = Excuse;
