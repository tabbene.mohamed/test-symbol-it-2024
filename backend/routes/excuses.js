const express = require('express');
const router = express.Router();
const { getExcuses, addExcuse } = require('../controllers/excuseController');

router.get('/', getExcuses);
router.post('/', addExcuse);

module.exports = router;
