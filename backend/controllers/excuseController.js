const Excuse = require('../models/Excuse');

exports.getExcuses = async (req, res) => {
  try {
    const excuses = await Excuse.findAll();
    res.json(excuses);
  } catch (error) {
    res.status(500).json({ error: 'Something went wrong' });
  }
};

exports.addExcuse = async (req, res) => {
  try {
    const { http_code, tag, message } = req.body;
    const excuse = await Excuse.create({ http_code, tag, message });
    res.status(201).json(excuse);
  } catch (error) {
    res.status(500).json({ error: 'Something went wrong' });
  }
};
