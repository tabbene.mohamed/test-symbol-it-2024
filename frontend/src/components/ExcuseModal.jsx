import React from 'react';

const ExcuseModal = ({ isOpen, onClose, onSave, onChange, newExcuse, newTag, newHttpCode, isAdding }) => {
  if (!isOpen) return null;

  return (
    <div className="modal">
      <div className="modal-content">
        <h2>Add New Excuse</h2>
        <input
          type="number"
          value={newHttpCode}
          onChange={(e) => onChange('http_code', e.target.value)}
          placeholder="HTTP Code"
        />
        <input
          type="text"
          value={newTag}
          onChange={(e) => onChange('tag', e.target.value)}
          placeholder="Tag"
        />
        <input
          type="text"
          value={newExcuse}
          onChange={(e) => onChange('message', e.target.value)}
          placeholder="Excuse Message"
        />
        <button onClick={onSave} disabled={isAdding}>
          {isAdding ? 'Adding...' : 'Add Excuse'}
        </button>
        <button onClick={onClose}>Close</button>
      </div>
    </div>
  );
};

export default ExcuseModal;
