import React from 'react';

function ErrorPage() {
  return (
    <div className="ErrorPage">
      <p>Page d’erreur 404</p>
    </div>
  );
}

export default ErrorPage;
