import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { fetchExcuses } from '../utils/api';

function MessagePage() {
  const { http_code } = useParams();
  const [excuses, setExcuses] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);

  useEffect(() => {
    const loadExcuses = async () => {
      try {
        const data = await fetchExcuses();
        setExcuses(data);
      } catch (error) {
        setError(error.message);
      } finally {
        setLoading(false);
      }
    };

    loadExcuses();
  }, []);

  const getExcuseByHttpCode = (httpcode) => {
    const excuse = excuses.find(excuse => excuse.http_code === httpcode);
    return excuse ? excuse.message : null;
  };

  const message = getExcuseByHttpCode(parseInt(http_code));

  return (
    <div className="MessagePage">
      <p><b>Code</b> : {http_code}</p>
      {loading ? (
        <p>Loading...</p>
      ) : error ? (
        <p style={{ color: 'red' }}>{error}</p>
      ) : message ? (
        <p><b>Message</b>: {message}</p>
      ) : (
        <p>No message found for this HTTP code</p>
      )}
    </div>
  );
}

export default MessagePage;
