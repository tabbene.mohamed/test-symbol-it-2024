import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

const LostPage = () => {
  const navigate = useNavigate();

  useEffect(() => {
    const timer = setTimeout(() => {
      navigate('/');
    }, 5000);

    return () => clearTimeout(timer);
  }, [navigate]);

  return (
    <div className='LostPage'>
      <h2>I’m Lost</h2>
      <img
        src="../../public/Loading_2.gif"
        alt="Loading..."
        style={{ width: '200px', height: '200px' }}
      />
    </div>
  );
};

export default LostPage;
