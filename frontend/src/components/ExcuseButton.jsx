import React from 'react';

const ExcuseButton = React.memo(({ generateExcuse }) => {
  return (
    <button onClick={generateExcuse}>Generate Excuse</button>
  );
});

export default ExcuseButton;
