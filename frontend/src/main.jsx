import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import App from './App';
import LostPage from './components/LostPage';
import MessagePage from './components/MessagePage.jsx';
import ErrorPage from './components/ErrorPage';
import './index.css';

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
   <Router>
      <Routes>
        <Route path="/" element={<App />} />
        <Route path="/lost" element={<LostPage />} />
        <Route path="/:http_code" element={<MessagePage />} />
        <Route path="*" element={<ErrorPage />} />
      </Routes>
    </Router>
  </React.StrictMode>
);

