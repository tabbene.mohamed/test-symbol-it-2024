// src/utils/api.test.jsx
import { describe, it, expect, vi, beforeEach, afterEach } from 'vitest';
import axios from 'axios';
import { fetchExcuses, addExcuse } from './api';

vi.mock('axios');

describe('api utils', () => {
  beforeEach(() => {
    vi.clearAllMocks();
  });

  describe('fetchExcuses', () => {
    it('fetches excuses successfully', async () => {
      const excuses = [{ id: 1, http_code: 701, message: 'Meh' }];
      axios.get.mockResolvedValue({ data: excuses });

      const data = await fetchExcuses();

      expect(axios.get).toHaveBeenCalledWith(`${import.meta.env.VITE_BASE_URL}/excuses`);
      expect(data).toEqual(excuses);
    });

    it('throws an error when fetching fails', async () => {
      axios.get.mockRejectedValue(new Error('Network Error'));

      await expect(fetchExcuses()).rejects.toThrow('Error fetching excuses');
    });
  });

  describe('addExcuse', () => {
    it('adds an excuse successfully', async () => {
      const newExcuse = { http_code: 702, message: 'Emacs' };
      axios.post.mockResolvedValue({ data: newExcuse });

      const data = await addExcuse(newExcuse);

      expect(axios.post).toHaveBeenCalledWith(`${import.meta.env.VITE_BASE_URL}/excuses`, newExcuse);
      expect(data).toEqual(newExcuse);
    });

    it('throws an error when adding fails', async () => {
      axios.post.mockRejectedValue(new Error('Network Error'));

      const newExcuse = { http_code: 702, message: 'Emacs' };
      await expect(addExcuse(newExcuse)).rejects.toThrow('Error fetching excuses');
    });
  });
});
