import axios from 'axios';

export const fetchExcuses = async () => {
  try {
    const response = await axios.get(`${import.meta.env.VITE_BASE_URL}/excuses`);
    return response.data;
  } catch (error) {
    throw new Error('Error fetching excuses');
  }
};

export const addExcuse = async (excuse) => {
  try {
  const response = await axios.post(`${import.meta.env.VITE_BASE_URL}/excuses`, excuse);
  return response.data;
} catch (error) {
  throw new Error('Error fetching excuses');
}
};

