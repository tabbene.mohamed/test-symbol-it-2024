import React, { useState, useEffect, useCallback } from 'react';
import { fetchExcuses, addExcuse } from './utils/api';
import ExcuseButton from './components/ExcuseButton';
import ExcuseModal from './components/ExcuseModal';
import './App.css';

function App() {
  const [excuses, setExcuses] = useState([]);
  const [currentExcuse, setCurrentExcuse] = useState('');
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [modalOpen, setModalOpen] = useState(false);
  const [newExcuse, setNewExcuse] = useState('');
  const [newTag, setNewTag] = useState('');
  const [newHttpCode, setNewHttpCode] = useState('');
  const [isAdding, setIsAdding] = useState(false);

  useEffect(() => {
    const loadExcuses = async () => {
      try {
        const data = await fetchExcuses();
        setExcuses(data);
      } catch (error) {
        setError(error.message);
      } finally {
        setLoading(false);
      }
    };

    loadExcuses();
  }, []);

  const generateExcuse = useCallback(() => {
    if (excuses.length === 0) return;
    const randomIndex = Math.floor(Math.random() * excuses.length);
    setCurrentExcuse(excuses[randomIndex].message);
  }, [excuses]);

  
  const handleAddExcuse = async () => {
    if (!newExcuse || !newTag || !newHttpCode) return;

    setIsAdding(true);
    const delay = Math.floor(Math.random() * 5000) + 1000;
    
    setTimeout(async () => {
      try {
        const response = await addExcuse({ http_code: newHttpCode, tag: newTag, message: newExcuse });
        setExcuses([...excuses, response.data]);
        setNewExcuse('');
        setNewTag('');
        setNewHttpCode('');
        setModalOpen(false);
      } catch (error) {
        setError('Error adding excuse');
      } finally {
        setIsAdding(false);
      }
    }, delay);
  };

  return (
    <div className="App">
      <h1>Les excuses de dev</h1>
      {loading ? (
        <p>Loading...</p>
      ) : error ? (
        <p style={{ color: 'red' }}>{error}</p>
      ) : excuses.length === 0 ? (
        <p>No excuses available</p>
      ) : (
        <>
          <p>{currentExcuse}</p>
          <ExcuseButton generateExcuse={generateExcuse} />
          <button onClick={() => setModalOpen(true)}>Add Excuse</button>
        </>
      )}
      <ExcuseModal
        isOpen={modalOpen}
        onClose={() => setModalOpen(false)}
        onSave={handleAddExcuse}
        onChange={(field, value) => {
          if (field === 'http_code') setNewHttpCode(value);
          else if (field === 'tag') setNewTag(value);
          else if (field === 'message') setNewExcuse(value);
        }}
        newExcuse={newExcuse}
        newTag={newTag}
        newHttpCode={newHttpCode}
        isAdding={isAdding}
      />
    </div>
  );
}

export default App;
